package dirs.reader.FileManager;

import dirs.reader.MyFile.MyFile;
import dirs.reader.MyFile.MyFileComparatorPath;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

public class FileManager {
    private Set<MyFile> listFiles = new HashSet<>();
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    public void makeListFiles(File folder) throws NullPointerException {

        for (File file : folder.listFiles()) {
            if(file.isDirectory()){
                makeListFiles(file);
            } else {
                listFiles.add(new MyFile(file.getName(), file.getParent().concat("/"), simpleDateFormat.format(file.lastModified())));
            }
        }
    }

    public void printDirList(String sortMode, String propertyMode){
        //sorted + get properties
        if(sortMode.equalsIgnoreCase("y") && propertyMode.equalsIgnoreCase("y")){
            listFiles.stream()
                    .sorted(new MyFileComparatorPath())
                    .forEach(f -> System.out.println(f.getsPath() + f.getsName() + " " + f.getiData()));
        //only sorted
        } else if(sortMode.equalsIgnoreCase("y")){
            listFiles.stream().sorted(new MyFileComparatorPath())
                    .forEach(f -> System.out.println(f.getsPath() + f.getsName()));
        //only getproperties
        } else if(propertyMode.equalsIgnoreCase("y")){
            listFiles.stream()
                    .forEach(f -> System.out.println(f.getsPath() + f.getsName() + " " + f.getiData()));
        //only file list
        } else {
            listFiles.stream().forEach(f -> System.out.println(f.getsPath() + f.getsName()));
        }
    }
}