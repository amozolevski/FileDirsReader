package dirs.reader.MyFile;

public class MyFile {
    private String sName;
    private String sPath;
    private String sData;

    public MyFile(String sName, String sPath, String sData) {
        this.sName = sName;
        this.sPath = sPath;
        this.sData = sData;
    }

    public String getsName() {
        return sName;
    }

    public String getsPath() {
        return sPath;
    }

    public String getiData(){
        return sData;
    }
}
