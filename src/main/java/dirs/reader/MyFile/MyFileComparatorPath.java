package dirs.reader.MyFile;

import java.util.Comparator;

public class MyFileComparatorPath implements Comparator<MyFile> {

    @Override
    public int compare(MyFile o1, MyFile o2) {
        //first compare the Path -> then the file's name
        if(o1.getsPath().equals(o2.getsPath()))
            return o1.getsName().compareTo(o2.getsName());

        return o1.getsPath().compareTo(o2.getsPath());
    }

}
