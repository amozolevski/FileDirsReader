package dirs.reader;

import dirs.reader.FileManager.FileManager;

import java.io.*;

public class App {

    public static void main(String[] args){

//      /media/tol/Data/IdeaProjects/FileDirsReader/

        BufferedReader bufferedReader = null;
        String filePath = null;
        String sortMode = null;
        String propertyMode = null;
        File file = null;
        FileManager fileManager = null;

        try {
            bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter directory path('q' to exit): ");
            filePath = bufferedReader.readLine();

            if(filePath.equalsIgnoreCase("q")) {
                System.out.println("quit program");
                return;
            }

            file = new File(filePath);

            if(file.exists()){
                fileManager = new FileManager();
                fileManager.makeListFiles(file);
            } else {
                System.out.println(file + " did not exist.");
                return;
            }

            System.out.println("Enter sort files mode (y/n or 'q' to exit):");
            sortMode = bufferedReader.readLine();

            if(sortMode.equalsIgnoreCase("q")) {
                System.out.println("quit program");
                return;
            }

            System.out.println("Enter property mode (y/n or 'q' to exit):");
            propertyMode = bufferedReader.readLine();

            if(propertyMode.equalsIgnoreCase("q")) {
                System.out.println("quit program");
                return;
            }

            fileManager.printDirList(sortMode, propertyMode);

        } catch (NullPointerException npe){
            System.err.println("Error " + npe.getMessage());
        } catch (IOException ioe) {
            System.err.println("Error " + ioe.getMessage());
        } finally {
            if(bufferedReader != null){
                try{
                    bufferedReader.close();
                } catch (IOException ioe) {
                    System.err.println("Error: " + ioe.getMessage());
                }
            }
        }
    }
}